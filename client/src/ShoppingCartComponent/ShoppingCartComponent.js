import '../css/App.css';
import {Header} from "../_Common/Header.js"
import {Footer} from "../_Common/Footer.js"
import {Link} from "react-router-dom"
import {formatPrice} from "../utils.js"
import {useEffect, useState } from 'react';

export function ShoppingCartComponent() {

    document.title="OnlineShop - Panier";
    const [shoppingCartItems, setShoppingCartItems] = useState([]);
    const [total, setOrdersAndShoppingCartTotal] = useState(0);
    const [emptyShoppingCart, IsEmptyShoppingCart] = useState(true);
    const [shoppingCartItemsLength, setshoppingCartItems] = useState(0);
    let htmlContent;

    function numbreOfItems(listItems){
        let nbItems = 0;
        listItems.forEach((item) => {
            nbItems += item.quantity;
        });
        return nbItems;
    }

    function updateTotal() {
        let calcTotal = 0.0;
        shoppingCartItems.forEach((orderItem) => {
            calcTotal += orderItem.quantity * orderItem.product.price;
        });
        setOrdersAndShoppingCartTotal(calcTotal);
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const shoppingCartList = await fetch("http://localhost:4000/api/shopping-cart", {credentials: 'include' });
                const productsList = await fetch("http://localhost:4000/api/products", {credentials: 'include' });
                
                if(shoppingCartList.ok && productsList.ok) {
                    const shoppingCartItems = await shoppingCartList.json();
                    const productsItems = await productsList.json();
                    let totalPrice = 0.0;
                    let shoppingCartListItems = [];
                    
                    shoppingCartItems.forEach((orderItem) => {
                        const shoppingCartItem = {
                            product: productsItems.find(item => parseInt(item.id) === parseInt(orderItem.productId)),
                            quantity: orderItem.quantity
                        };
                        shoppingCartListItems.push(shoppingCartItem);
                        totalPrice += shoppingCartItem.quantity * shoppingCartItem.product.price;
                    });
                    if (shoppingCartListItems.length !== 0) {
                        IsEmptyShoppingCart(false);
                    }
                    setShoppingCartItems(shoppingCartListItems);
                    setOrdersAndShoppingCartTotal(totalPrice);
                    setshoppingCartItems(numbreOfItems(shoppingCartItems));
                } 
                
                else {
                    throw shoppingCartList.json();
                }
            } 
            
            catch(e) {
                console.error(e);
            }
        }
        fetchData();
    }, []);

    async function updateNbItemsInShoppingCart(shoppingCartItem) {
        const shoppingCartProduct = await fetch(`http://localhost:4000/api/shopping-cart/${shoppingCartItem.product.id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: 'include',
            body: JSON.stringify({productId: shoppingCartItem.product.id, quantity: shoppingCartItem.quantity})
        });

        if(shoppingCartProduct.ok) {
            const newShoppingCartItems = [...shoppingCartItems];
            newShoppingCartItems.find((selectedItem) => parseInt(selectedItem.product.id) === parseInt(shoppingCartItem.product.id)).quantity = shoppingCartItem.quantity;
            setShoppingCartItems(newShoppingCartItems);
            setshoppingCartItems(numbreOfItems(newShoppingCartItems));
            updateTotal();
        }
    };

    async function removeItemInShoppingCart(shoppingCartItem) {
        if (window.confirm("Voulez-vous supprimer le produit du panier?")){
            const shoppingCartProduct = await fetch(`http://localhost:4000/api/shopping-cart/${shoppingCartItem.product.id}`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                },
                credentials: 'include'
            });

            if(shoppingCartProduct.ok) {
                const newItems = shoppingCartItems.filter((orderitem) => orderitem !== shoppingCartItem);
                setShoppingCartItems(newItems);

                if (newItems.length === 0) {
                    IsEmptyShoppingCart(true);
                }

                updateTotal();
                setshoppingCartItems(numbreOfItems(newItems));
            }
        }
    };

    async function deleteAllItemsShoppingCart() {
        if (window.confirm("Voulez-vous supprimer tous les produits du panier?")) {
            const shoppingCartItems = await fetch(`http://localhost:4000/api/shopping-cart`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                },
                credentials: 'include'
            });
            if (shoppingCartItems.ok) {
                setshoppingCartItems(0);
                IsEmptyShoppingCart(true);
            }
        }
    }

    if (emptyShoppingCart) {
        htmlContent = (
            <article>
                <h1>Panier</h1>
                <div id="shopping-cart-container">
                    <p>Aucun produit dans le panier.</p>
                </div>
            </article>
        )
    } 
    else {
        htmlContent = (
            <article>
                <h1>Panier</h1>
                <div id="shopping-cart-container">
                    <table className="table shopping-cart-table">
                        <thead>
                            <tr>
                            <th></th>
                            <th>Produit</th>
                            <th>Prix unitaire</th>
                            <th>Quantité</th>
                            <th>Prix</th>
                            </tr>
                        </thead>
                        <tbody>
                        {shoppingCartItems.map(item => 
                            <tr key={item.product.id}>
                                <td><button className="remove-item-button" title="Supprimer" 
                                onClick={() => removeItemInShoppingCart(item)}><i className="fa fa-times"></i></button></td>

                                <td><Link to={`./product/${item.product.id}`}>{item.product.name}</Link></td>
                                
                                <td>{formatPrice(item.product.price)}</td>

                                <td>
                                    <div className="row">

                                    <div className="col"><button className="remove-quantity-button" 
                                    title="Retirer" onClick={() => {item.quantity = item.quantity - 1; 
                                    updateNbItemsInShoppingCart(item)}} disabled={item.quantity <= 1 ? "disabled" : ""}>

                                    <i className="fa fa-minus"></i></button></div>

                                    <div className="col quantity">{item.quantity}</div>

                                    <div className="col"><button className="add-quantity-button" 
                                    title="Ajouter" onClick={() => {item.quantity = item.quantity + 1; 
                                    updateNbItemsInShoppingCart(item)}}><i className="fa fa-plus"></i></button></div>
                                    </div>
                                </td>
                                <td className="price">{formatPrice(item.product.price * item.quantity)}</td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                    <p className="shopping-cart-total">Total: <strong id="total-amount">{formatPrice(total)}</strong></p>
                    <a className="btn pull-right" href="./commande">Commander <i className="fa fa-angle-double-right"></i></a>
                    
                    <button className="btn" id="remove-all-items-button" 
                    onClick={() => deleteAllItemsShoppingCart()}>
                    <i className="fa fa-trash-o"></i>&nbsp; Vider le panier</button>
                </div>
            </article>
        );
    }
    return (
        <div>
            <Header cartCount={shoppingCartItemsLength}/>
            <main>
                {htmlContent}
            </main>
            <Footer/>
        </div>
    );
}