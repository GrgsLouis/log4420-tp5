import '../css/App.css';
import {Header} from "../_Common/Header.js"
import {Footer} from "../_Common/Footer.js"
import { SingleProductPart } from './SingleProductPart';
import { useEffect, useState } from 'react';
import { applyCategory, applySortingCriteria } from "./ProductsUtil.js"

export function ProductsComponent() {

    document.title="OnlineShop - Produits";
    const [ category, setCategory] = useState("all")
    const [ sortType, setSortType ] = useState("price-asc");
    const [productsItems, setProductsItems] = useState([]);
    const [ currentProductList, setCurrentProductList] = useState(productsItems);
    const [loading, setLoading] = useState(true);
    const [shoppingCartItemsLength, setShoppingCartItems] = useState(0);
    
    const categories = [
        {id: "cameras", text: "Appareils photo"},
        {id: "consoles", text: "Consoles"},
        {id: "screens", text: "Écrans"},
        {id: "computers", text: "Ordinateurs"},
        {id: "all", text: "Tous les produits"}
    ];

    const sorts = [
        {id: "price-asc", text: "Prix (bas-haut)"},
        {id: "price-dsc", text: "Prix (haut-bas)"},
        {id: "alpha-asc", text: "Nom (A-Z)"},
        {id: "alpha-dsc", text: "Nom (Z-A)"},
    ]

    function numbreOfItems(listItems){
        let nbItems = 0;
        listItems.forEach((item) => {
            nbItems += item.quantity;
        });
        return nbItems;
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const shoppingCartItems = await fetch("http://localhost:4000/api/shopping-cart", {credentials: 'include' });
                if(shoppingCartItems.ok) {
                    const shoppingItemsList = await shoppingCartItems.json();
                    setShoppingCartItems(numbreOfItems(shoppingItemsList));
                } else {
                    throw shoppingCartItems.json();
                }
            } catch(e) {
                console.error(e);
            }
        }
        fetchData();
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const productsItems = await fetch("http://localhost:4000/api/products");
                if(productsItems.ok) {
                    setProductsItems(await productsItems.json());
                } else {
                    throw productsItems.json();
                }
            } 
            catch(e) {
                console.error(e);
            }
            setLoading(false);
        }
        fetchData();
    }, []);
    
    useEffect(() => {

        if (!productsItems?.length) {
            return;
        }

        const newProductsItems = applyCategory(productsItems, category);
        applySortingCriteria(newProductsItems, sortType);
        setCurrentProductList(newProductsItems)
    }, [productsItems, category, sortType]);


    return (
        <div>
            <Header currentActive="product" cartCount={shoppingCartItemsLength}/>
            <main>
            <section className="sidebar" aria-label="Filtres">
            <section>
                <h2>Catégories</h2>
                <div className="btn-group vertical" id="product-categories">
                    {categories.map(catObj => 
                        <button 
                            key={catObj.id}
                            className={category===catObj.id ? "selected" : ""}
                            data-category={catObj.id}
                            onClick={() => setCategory(catObj.id)}
                        >
                            {catObj.text}
                        </button>
                    )}
                </div>
            </section>
            <section>
                <h2>Classement</h2>
                <div className="btn-group vertical" id="product-criteria">
                    {sorts.map(sortObj => 
                        <button 
                            key={sortObj.id}
                            className={sortType===sortObj.id ? "selected" : ""}
                            data-category={sortObj.id}
                            onClick={() => setSortType(sortObj.id)}
                        >
                            {sortObj.text}
                        </button>
                    )}
                </div>
            </section>
            </section>
            <article>
                <span className="right-header" id="products-count">{currentProductList.length} produits</span>
                <h1>Produits</h1>
                <div className="products" id="products-list">
                    {loading && <div className="loading"></div>}
                    {
                        currentProductList.map(p => 
                            <SingleProductPart key={p.id} productData={p}/>
                        )
                    }
                </div>
            </article>
        </main>
            <Footer/>
        </div>
    );
}