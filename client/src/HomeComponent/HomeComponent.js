import '../css/App.css';
import home from "../img/home.png";
import {Header} from "../_Common/Header.js"
import {Footer} from "../_Common/Footer.js"
import {Link} from "react-router-dom"
import { useEffect, useState } from 'react';

export function HomeComponent() {
    document.title="OnlineShop - Accueil";
    const [shoppingCartItemsLength, setShoppingCartItems] = useState(0);

    function numbreOfItems(listItems){
        let nbItems = 0;
        listItems.forEach((item) => {
            nbItems += item.quantity;
        });
        return nbItems;
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const shoppingCartItems = await fetch("http://localhost:4000/api/shopping-cart", {credentials: 'include' });
                if(shoppingCartItems.ok) {
                    const shoppingCartList = await shoppingCartItems.json();
                    setShoppingCartItems(numbreOfItems(shoppingCartList));
                } else {
                    throw shoppingCartItems.json();
                }
            } 
            catch(e) {
                console.error(e);
            }
        }
        fetchData();
    }, []);

    return (
        <div>
            <Header currentActive="home" cartCount={shoppingCartItemsLength}/>
            <main>
                <article>
                    <img alt="home" src={home} id="home-img"/>
                    <h1>Le site n&deg;1 pour les achats en ligne!</h1>
                    <p>Découvrez nos différents produits au meilleur prix.</p>
                    <Link className="btn" to="/products">En savoir plus <i className="fa fa-angle-double-right"></i></Link>
                </article>
            </main>
            <Footer/>
        </div>
    );
}
