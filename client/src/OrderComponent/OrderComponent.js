import '../css/App.css';
import {Header} from "../_Common/Header.js"
import {Footer} from "../_Common/Footer.js"
import {useState, useEffect, useCallback} from 'react';


export function OrderComponent() {
    
    document.title="OnlineShop - Commande";
    const [shoppingCartNbItems, setShoppingCartNbItems] = useState(0);
    
    const [order, setOrder] = useState({
        "id" : 0,
        "firstName" : "",
        "lastName" : "",
        "email" : "",
        "phone" : "",
        "products" : []
    });

    const formSubmit = (event) => {
      return fetch("http://localhost:4000/api/orders", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(order),
        credentials: 'include'
      });
    }

    function numbreOfItems(listItems){
        let nbItems = 0;
        listItems.forEach((item) => {
            nbItems += item.quantity;
        });
        return nbItems;
    }

    const changeProperty = useCallback((name, value) => {
      console.log(name + " " +value);
      const newOrder = order;
      newOrder[name] = value;  
      setOrder(newOrder);
    }, [order])
        
    useEffect(() => {
        const getShoppingCartData = async () => {
            try {
                const shoppingCartAnswer = await fetch("http://localhost:4000/api/shopping-cart", {credentials: 'include'});
                
                if(shoppingCartAnswer.ok) {
                    const shoppingCartItems = await shoppingCartAnswer.json();
                    setShoppingCartNbItems(numbreOfItems(shoppingCartItems));
                    changeProperty("products", shoppingCartItems.map(item => {
                        return {
                          id: item.productId,
                          quantity: item.quantity
                        }
                      }));
                } 
                
                else {
                    throw shoppingCartAnswer.json();
                }
            } 
            catch(e) {
                console.error(e);
            }
        }
        const getOrderData = async () => {
          try {
              const ordersAnswer = await fetch("http://localhost:4000/api/orders");
              
              if(ordersAnswer.ok) {
                  const orders = await ordersAnswer.json();
                  changeProperty("id", orders.length);
              } 
              
              else {
                  throw ordersAnswer.json();
              }
          } 
          catch(e) {
              console.error(e);
          }
        }
        getShoppingCartData();
        getOrderData();
    }, [changeProperty]);

    return (
        <div>
            <Header cartCount={shoppingCartNbItems}/>
            <main>
                <article>
                  <h1>Commande</h1>
                  <form id="order-form" action="/confirmation" method="get" 
                  onSubmit={formSubmit}>
                    <section>
                      <h2>Contact</h2>
                      <div className="row">
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="firstName">Prénom</label>
                            <input className="form-control" type="text" id="firstName" 
                            name="firstName" placeholder="Prénom" minLength="2" 
                            onChange={e => changeProperty(e.target.name, e.target.value)} required/>
                          </div>
                        </div>
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="lastName">Nom</label>
                            <input className="form-control" type="text" id="lastName" 
                            name="lastName" placeholder="Nom" minLength="2" 
                            onChange={e => changeProperty(e.target.name, e.target.value)} required/>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="email">Adresse courriel</label>
                            <input className="form-control" type="email" id="email" 
                            name="email" placeholder="Adresse courriel" onChange={e => 
                            changeProperty(e.target.name, e.target.value)} required/>
                          </div>
                        </div>
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="phone">Téléphone</label>
                            <input className="form-control" type="tel" id="phone" pattern="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$"
                            name="phone" placeholder="###-###-####" onChange={e => changeProperty(e.target.name, e.target.value)} required/>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section>
                      <h2>Paiement</h2>
                      <div className="row">
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="credit-card">Numéro de carte de crédit</label>
                            <input className="form-control" type="text" id="credit-card" 
                            name="credit-card" placeholder="•••• •••• •••• ••••" required pattern="^[0-9]{16}$"
                            />
                          </div>
                        </div>
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="credit-card-expiry">Expiration (mm/aa)</label>
                            <input className="form-control" type="text" id="credit-card-expiry" 
                            name="credit-card-expiry" placeholder="mm/aa" required
                            pattern="^(0[1-9]|1[0-2])\/?([0-9]{2})"/>
                          </div>
                        </div>
                      </div>
                    </section>
                    <button className="btn pull-right" type="submit">Payer <i className="fa fa-angle-double-right"></i></button>
                  </form>
                </article>
              </main>
            <Footer/>
        </div>
    );
}