import '../css/App.css';
import {Header} from "../_Common/Header.js"
import {Footer} from "../_Common/Footer.js"
import {useParams} from "react-router-dom";
import {imageMap} from "../ProductsComponent/ProductImageLoader";
import {formatPrice } from "../utils.js"
import {useEffect, useState } from 'react';

export function ProductComponent() {
    document.title="OnlineShop - Produit";
    const { id } = useParams();

    const [product, setProductsItem] = useState();
    const [loading, setLoading] = useState(true);
    const [itemQuantity, setItemQuantity] = useState(1);
    const [dialogShowing, setShowDialog] = useState(false);
    const [shoppingCartItemsLength, setShoppingCartItems] = useState(0);
    let htmlContent;

    function numbreOfItems(listItems){
        let nbItems = 0;
        listItems.forEach((item) => {
            nbItems += item.quantity;
        });
        return nbItems;
    }

    useEffect(() => {
        const fetchData = async () => {
            
            try {
                const shoppingCartItems = await fetch("http://localhost:4000/api/shopping-cart", {credentials: 'include' });
                if(shoppingCartItems.ok) {
                    const shoppingCartList = await shoppingCartItems.json();
                    setShoppingCartItems(numbreOfItems(shoppingCartList));
                } else {
                    throw shoppingCartItems.json();
                }
            } 
            catch(e) {
                console.error(e);
            }
        }
        fetchData();
    }, []);
    
    useEffect(() => {
        const fetchData = async () => {
            
            try {
                const productsItem = await fetch(`http://localhost:4000/api/products/${id}`, {credentials: 'include'});
                if(productsItem.ok) {
                    setProductsItem(await productsItem.json());
                } else {
                    throw productsItem.json();
                }
            } 
            catch(e) {
                console.error(e);
            }
            setLoading(false);
        }
        fetchData();
    }, [id]);

    async function submit(event) {

        event.preventDefault();
        let addProductToShoppingCart = { productId: parseInt(id), quantity: parseInt(itemQuantity)};
        
        const shoppingCartItems = await fetch("http://localhost:4000/api/shopping-cart", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: 'include',
            body: JSON.stringify(addProductToShoppingCart)
        });

        if(shoppingCartItems.ok) {
            setShowDialog(true);
            setTimeout(() => { setShowDialog(false)}, 5000);
            setShoppingCartItems(shoppingCartItemsLength + itemQuantity);
        } 
        
        else {
            const productGet = await fetch(`http://localhost:4000/api/shopping-cart/${id}`, {credentials: 'include'})
            
            if (productGet.ok) {
                const existingProduct = await productGet.json();
                
                const updateItemShoppingCart = await fetch(`http://localhost:4000/api/shopping-cart/${id}`, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    credentials: 'include',
                    body: JSON.stringify({quantity: parseInt(itemQuantity) + parseInt(existingProduct.quantity)})
                });
                
                if (updateItemShoppingCart.ok) {
                    setShowDialog(true);
                    setTimeout(() => { setShowDialog(false)}, 5000);
                    setShoppingCartItems(shoppingCartItemsLength + parseInt(itemQuantity));
                } 
            }
        }
    };

    if(loading) {
        htmlContent = (
            <article>
                <div className="loading"></div>
            </article>
        )
    } else if(product) {
        htmlContent = (
            <article>
                <h1 id="product-name">{product.name}</h1>
                <div className="row">
                    <div className="col">
                        <img alt="product" src={imageMap[product.image]} id="product-image"/>
                    </div>
                    <div className="col">
                        <section>
                            <h2>Description</h2>
                            <p id="product-desc" dangerouslySetInnerHTML={{__html: product.description}}></p>
                        </section>
                        <section>
                            <h2>Caractéristiques</h2>
                            <ul id="product-features">
                                {product.features.map(feature => (
                                    <li key={feature}>{feature}</li>    
                                ))}
                            </ul>
                        </section>
                        <hr/>
                        <form className="pull-right" id="add-to-cart-form">
                            <label htmlFor="product-quantity">Quantité:</label>
                            <input className="form-control" type="number" defaultValue="1" min="1" id="product-quantity"
                            onChange={(event) => setItemQuantity(event.target.value)}/>
                            
                            <button className="btn" title="Ajouter au panier" type="submit" 
                            onClick={(event) => submit(event)}>
                            <i className="fa fa-cart-plus"></i>&nbsp; Ajouter
                            </button>
                        </form>
                        <p>Prix: <strong id="product-price">{formatPrice(product.price)}</strong></p>
                    </div>
                </div>
                <div className={dialogShowing ? "dialog fadeIn" : "dialog fadeOut"} 
                id="dialog"> Le produit a été ajouté au panier.
                </div>
            </article>
        );
    } else {
        console.error("Invalid ID specified");
        htmlContent =  (
            <article>
                <h1>Produit non trouvée!</h1>
            </article>
        );
    }
    return (
        <div>
            <Header currentActive="product" cartCount={shoppingCartItemsLength}/>
            <main>
                {htmlContent}
            </main>
            <Footer/>
        </div>
    );
}