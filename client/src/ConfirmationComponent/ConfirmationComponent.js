import '../css/App.css';
import {Header} from "../_Common/Header.js"
import {Footer} from "../_Common/Footer.js"
import { useEffect, useState } from 'react';

export function ConfirmationComponent() {
  document.title="OnlineShop - Commande";
  const [name, setName] = useState("");
  const [confirmationNumber, setConfirmationNumber] = useState(0);

  useEffect(() => {
    const deleteShoppingCartItems = async () => {
      try {
        await fetch(`http://localhost:4000/api/shopping-cart`, {
          method: "DELETE",
          headers: {
              "Content-Type": "application/json",
          },
          credentials: 'include'
        });
      }
      catch(e) {
          console.error(e);
      }
    }
    const fetchOrderData = async () => {
      try {
          const ordersItems = await fetch("http://localhost:4000/api/orders");
          
          if(ordersItems.ok) {
              const ordersList = await ordersItems.json();
              setConfirmationNumber(ordersList.length+1);
              
              try {
                const ordersItem = await fetch("http://localhost:4000/api/orders/" + confirmationNumber);
                
                if(ordersItem.ok) {
                    const order = await ordersItem.json();
                    console.log(order);
                    setName(order.firstName + " " + order.lastName );
                  } 
                  
                  else {
                    //throw ordersItem.json();
                }
            }
            catch(e) {
                console.error(e);
            }
          }

          else {
              throw ordersItems.json();
          }
      } 
      catch(e) {
          console.error(e);
      }
    }
    deleteShoppingCartItems();
    fetchOrderData();
  }, [confirmationNumber]);

  return (
    <div>
      <Header/>
      <main>
        <article>
          <h1>Votre commande est confirmée <span id="name">{name}</span>!</h1>
          <p>Votre numéro de confirmation est le <strong id="confirmation-number">{confirmationNumber}</strong>.</p>
        </article>
      </main>
      <Footer/>
    </div>
  );
}